#![forbid(unsafe_code)]
#![cfg_attr(not(debug_assertions), deny(warnings))] // Forbid warnings in release builds
#![warn(clippy::all, rust_2018_idioms)]
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] //Hide console window in release builds on Windows, this blocks stdout.
use imclient_egui::ImClient;
use eframe::egui::Vec2;

// When compiling natively:
#[cfg(not(target_arch = "wasm32"))]
fn main() {
	let app = ImClient::default();
	let def = eframe::NativeOptions::default();
	let native_options = eframe::NativeOptions{
		initial_window_size: Some(Vec2{
			x: 700.0,
			y: 500.0
		}),
		.. def
	};
	eframe::run_native(Box::new(app), native_options);
}
