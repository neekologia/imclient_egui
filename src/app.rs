use eframe::{egui, epi};
use eframe::egui::TextBuffer;
use egui::{Key, Modifiers};
use egui::Separator;
use std::{
	io::{Write, Read},
	net::{TcpStream, ToSocketAddrs}
};

pub struct ImClient {
	buffer: Vec<String>,
	text: String,
	user: String,
	sent: bool,
	server: String,
	room: String,
	new_room: String,
	changed_room: bool,
	rooms: Vec<String>,
	id: usize,
	fc: usize,
	dark: bool,
}

impl Default for ImClient {
	fn default() -> Self { Self {
		buffer: vec![String::new()],
		text: "archneek.zapto.org:14114".to_string(),
		user: "anon".to_string(),
		sent: false,
		server: String::new(),
		room: String::from("entryway"),
		new_room: String::new(),
		changed_room: false,
		rooms: vec![String::from("entryway")],
		id: 0,
		fc: 300,
		dark: true,
	}}
}

impl ImClient {
	fn connect(&mut self) {
		// check server address
		let addr = match self.server.to_socket_addrs() {
			Ok(mut a) => a.next().unwrap(),
			Err(_) => {
				self.server = String::new();
				return;
			}
		};

		// check connection and create stream
		let mut stream = match TcpStream::connect(addr) {
			Ok(s) => s,
			Err(_) => {
				self.server = String::new();
				return;
			}
		};

		if self.room == "" { return; }

		// write reply to server according to update bool
		let reply = { if !self.sent {
			// check for new messages
			[ "update", &self.id.to_string(), &self.room ].join("\n")
		} else {
			// write a new message
			[ "input",
				[ &self.user.as_str(), ": ", &self.text.as_str() ].join("").as_str(),
			&self.room ].join("\n")
		}};
		stream.write(reply.as_bytes()).unwrap();
		
		// read update from server
		let mut bufread = [0; 64000];
		let data = stream.read(&mut bufread[0..64000]);
		match data { Ok(data) => {
			// check if red data is > 1 lines and contains an id
			let input = String::from_utf8_lossy(&bufread[0..data]).to_string();
			if input.lines().count() > 1 {
				let mut lines = input.lines();
				match lines.next().unwrap().parse::<usize>() { Ok(new_id) => {
					// save new id and strings
					self.id = new_id;
					for x in lines {
						self.buffer.push(x.to_string() + &"\n".to_string());
					}
				} Err(_) => (), }
			}
		} Err(_) => (), }
		self.sent = false;
	}
}

impl epi::App for ImClient {
	fn name(&self) -> &str {
		"imclient_egui"
	}

	// Called each timeethe UI needs repainting.
	fn update(&mut self, ctx: &egui::Context, _frame: &epi::Frame) {
		let mut style: egui::Style = (*ctx.style()).clone();
		
		// select style
		if style.visuals.dark_mode {
			self.dark = true;
			style.visuals.override_text_color = Some(egui::Color32::WHITE);
		} else {
			self.dark = false;
			style.visuals.override_text_color = Some(egui::Color32::BLACK);
		}
		ctx.set_style(style);
		ctx.request_repaint();

		if self.server == "" {
			// if server isn't set, display textbox to input one
			egui::CentralPanel::default().show(ctx, |ui| {
				ui.style_mut().override_text_style = Some(egui::TextStyle::Heading);
				ui.vertical_centered_justified(|ui| {
				ui.label("username");
				ui.text_edit_singleline(&mut self.user);
				ui.label("server");
				ui.text_edit_singleline(&mut self.text);
				if ui.button("join").clicked() {
					self.server = self.text.take();
					self.connect();
					self.text.clear();
				}});
			});
		} else {
			// show sidepanel with room list
			egui::SidePanel::left("rooms_panel").show(ctx, |ui| {
				ui.style_mut().override_text_style = Some(egui::TextStyle::Heading);
				ui.vertical_centered_justified(|ui| {
					for x in self.rooms.iter() {
						if x != "" {
							if ui.button(x).clicked() {
								self.room = x.to_string();
								self.buffer = vec![String::new()];
								self.id = 0;
								self.changed_room = true;
							}
						}
					}
					// textbox to add rooms at the bottom of sidepanel
					ui.with_layout(egui::Layout::bottom_up(egui::Align::BOTTOM), |ui| {
						ui.label("add a room");
						ui.text_edit_singleline(&mut self.new_room);
						if ui.input().key_pressed(Key::Enter) && self.new_room != "" {
							self.rooms.push(self.new_room.take());
						}
					});
				});
			});

			// if room is set connect to the server
			if self.room != "" {
				// if room changed get new messages
				if self.changed_room { self.connect(); self.changed_room = false; }
				self.fc += 1;
				// polling every second
				if self.fc > 60 { self.fc = 0; self.connect(); }
				// bottom panel to input messages
				egui::TopBottomPanel::bottom("").show(ctx, |ui| {
					ui.horizontal(|ui| {
						ui.label("Send a message: ");
						ui.text_edit_multiline(&mut self.text);
						if ui.input().key_pressed(Key::Enter)
						&& self.text.as_str().lines().collect::<Vec<&str>>().join("") != "" {
							if !ui.input_mut().consume_key(Modifiers::SHIFT, Key::Enter) {
								self.sent = true;
								self.connect();
								self.text.clear();
							}
						}
					});
					ui.add(Separator::default().horizontal());
					if self.dark {
						if ui.button("light").clicked() {
							ctx.set_visuals(egui::Visuals::light());
						}
					} else {
						if ui.button("dark").clicked() {
							ctx.set_visuals(egui::Visuals::dark());
						}
					}
				});

				// central panel to display messages
				egui::CentralPanel::default().show(ctx, |ui| {
					egui::ScrollArea::vertical()
					.stick_to_bottom().auto_shrink([false; 2]).show(ui, |ui| {
						ui.style_mut().override_text_style = Some(egui::TextStyle::Heading);
						ui.label(self.buffer.join(""));
					});
				});
			}
		}
	}
}
